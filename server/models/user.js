const config = require("config");
const jwt = require("jsonwebtoken");
const Joi = require("joi");

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: {
      type: DataTypes.STRING,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    isAdmin: {
      type: DataTypes.BOOLEAN,
    }
  });
  return User;
};
  generateAuthToken = function() {
    const token = jwt.sign(
      { _id: this._id, isAdmin: this.isAdmin },
      config.get("jwtPrivateKey")
    );
    return token;
  };
  validateUser = function(user){
    const schema = {
      name: Joi.string(),
      email: Joi.string()
        .email(),
      password: Joi.string()
    };
  
    return Joi.validate(user, schema);
  }

exports.generateAuthToken = generateAuthToken;
exports.validate = validateUser;